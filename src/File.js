export default function File(props) {
  return (
    <li>
      <span className="file-icon">
        {props.name}
      </span>
    </li>
  );
}
