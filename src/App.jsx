import React from 'react';

import Folder from './Folder';
import File from './File';
import './App.css';
export default function App() {
  return (
    <div className="tree-folder">
      <ul>
        {data.map((item, index) =>
          item.isDirectory ? (
            <Folder key={index} {...item} />
          ) : (
            <File key={index} {...item} />
          )
        )}
      </ul>
    </div>
  );
}

const data = [
  {
    name: 'Folder 1',
    isDirectory: true,
    folders: [
      { name: 'Sub Folder 1', isDirectory: true, folders: [] },
      {
        name: 'Sub Folder 2',
        isDirectory: true,
        folders: [
          { name: 'File 1', isDirectory: false },
          { name: 'File 2', isDirectory: false },
          { name: 'File 3', isDirectory: false },
          { name: 'File 4', isDirectory: false },
          { name: 'File 5', isDirectory: false },
          { name: 'File 6', isDirectory: false }
        ]
      },
      { name: 'Sub Folder 3', isDirectory: true, folders: [] }
    ]
  },
  { name: 'Folder 2', isDirectory: true, folders: [] },
  { name: 'File 1', isDirectory: false }
];
