import React from 'react';

export default class Test extends React.Component {
  constructor() {
    super();
    this.state = {
      name: 'Name1'
    };
  }

  render() {
    console.log(this.state.name);
    return (
      <div>
        <header className="page-header">
          <h1>Cute Puppies Express!</h1>
        </header>

        <main>
          <p>
            I love beagles <em>so</em> much! Like, really, a lot. They’re
            adorable and their ears are so, so snuggly soft!
          </p>
          <details>
            <summary>Details</summary>
            Something small enough to escape casual notice.
          </details>
        </main>

        <article>
          <p>
            The Disney movie <cite>The Little Mermaid</cite> was first released
            to theatres in 1989.
          </p>
          <aside>
            <p>The movie earned $87 million during its initial release.</p>
          </aside>
          <p>More info about the movie...</p>
        </article>
      </div>
    );
  }
}
