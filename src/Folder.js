import React, { useState } from 'react';
import File from './File';
function Folder(props) {
  const [expand, setExpand] = useState(false);
  const getClass = () => {
    return expand ? 'caret caret-down' : 'caret';
  };
  return (
    <>
      <li>
        <span className={getClass()} onClick={() => setExpand(!expand)}>
          <span className="folder-icon"></span>
          {props.name}
        </span>
      </li>
      {expand && (
        <ul>
          {props.folders.map((item, index) =>
            item.isDirectory ? (
              <Folder key={index} {...item} />
            ) : (
              <File key={index} {...item} />
            )
          )}
        </ul>
      )}
    </>
  );
}

export default Folder;
